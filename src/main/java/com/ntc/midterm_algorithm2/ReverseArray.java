package com.ntc.midterm_algorithm2;

import java.util.Scanner;

/**
 *
 * @author L4ZY
 */
public class ReverseArray {

    public static void main(String[] args) {
        long start, stop;
        start = System.nanoTime();
        Scanner kb = new Scanner(System.in);
        int[] arr;
        arr = new int[5];
        arr[0] = kb.nextInt();
        arr[1] = kb.nextInt();
        arr[2] = kb.nextInt();
        arr[3] = kb.nextInt();
        arr[4] = kb.nextInt();
        int length = arr.length - 1;
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }

        for (int i = 0; i < (length) / 2; i++) {
            int data = arr[i];
            arr[i] = arr[length - i];
            arr[length - i] = data;
        }
        System.out.println("");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        stop = System.nanoTime();
        System.out.println("");
        System.out.println("Running Time is " + (stop - start) * 1E-9 + " secs.");

    }

}
